<?php
// $Id$

/**
 * @file
 * Renders administrative pages for webform_signup module
 */

function webform_signup_admin_settings() {
  drupal_add_js(drupal_get_path('module', 'webform_signup') . '/webform_signup.admin.js');
  $form = array();

  /* Content type settings */
  $form['webform_signup_contenttype_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content type settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $content_types = node_get_types('names');
  $webform_types = drupal_map_assoc(webform_variable_get('webform_node_types'));
  $options = array_intersect_key($content_types, $webform_types);
  $form['webform_signup_contenttype_settings']['webform_signup_contenttypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Event content types'),
    '#description' => t('Specify the content type that will be used for the event functionality'),
    '#required' => TRUE,
    '#options' => $options,
    '#default_value' => array_filter(variable_get('webform_signup_contenttypes', array())),
  );

  /* Webform settings */
  $form['webform_signup_webform_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webform settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['webform_signup_webform_settings']['webform_signup_use_template_node'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use a node as template for automatically loading webform components'),
    '#default_value' => variable_get('webform_signup_use_template_node', 0),
  );

  $form['webform_signup_webform_settings']['placeholder'] = array(
    '#value' => 'TODO: add autocomplete node selection widget',
  );

  $form['webform_signup_webform_settings']['webform_signup_template_nid'] = array(
    '#type' => 'textfield',
    '#title' => t('Node ID for template node'),
    '#description' => t('Specify which node to use as template for automatically adding webform components to event nodes'),
    '#required' => FALSE,
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => variable_get('webform_signup_template_nid', ''),
  );

  $event_types = array_filter(variable_get('webform_signup_contenttypes', array()));
  $template_node_nid = variable_get('webform_signup_template_nid', '');

  /** Subscription limit settings */
  $form['webform_signup_limit_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription limit settings'),
    '#description' => t('Configure options for automatically mapping event node & webform submission limits') . '<br />' .
                      t('WARNING: this will automatically enforce Submission access control settings on your webforms based on the template node!'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if (!empty($event_types) && $template_node_nid > 0) {
    // check if webform settings are correct
    if (variable_get('webform_submission_access_control', 0) != 1) {
      drupal_set_message(t('WARNING: You need to enable <a href="!url">Submission access control</a> for the subscription limit functionality to work', array('!url' => url('admin/settings/webform'))), 'error');
    }

    $form['webform_signup_limit_settings']['webform_signup_use_subscription_limit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Activate form subscription limit based on CCK field value'),
      '#default_value' => variable_get('webform_signup_use_subscription_limit', 0),
    );

    $available_fields = webform_signup_get_content_fields('number_integer', TRUE);
    $form['webform_signup_limit_settings']['webform_signup_limit_field'] = array(
      '#type' => 'select',
      '#title' => t('Subscription limit field'),
      '#description' => t('Specify the CCK field that contains the max. number of signups that should be available for this event.'),
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#options' => $available_fields,
      '#default_value' => variable_get('webform_signup_limit_field', ''),
    );

    $form['webform_signup_limit_settings']['webform_signup_limit_count_method'] = array(
      '#type' => 'radios',
      '#title' => t('Subscription count method'),
      '#description' => t('Specify which method to use to limit event subscriptions. The number of subscriptions is limited based on the field specified above.'),
      '#required' => FALSE,
      '#options' => array(
        'default' => t('Default: one form submission equals one subscription.'),
        'component' => t('Webform component: a selected webform component value equals the number of subscriptions per form submission.'),
      ),
      '#default_value' => variable_get('webform_signup_limit_count_method', 'default'),
      '#weight' => 1,
    );

    $available_components = webform_signup_get_template_node_components();
    $form['webform_signup_limit_settings']['webform_signup_limit_component'] = array(
      '#type' => 'fieldset',
      '#title' => t('Webform component count method settings'),
      '#description' => t('Settings to be used when "Webform component" has been selected as <em>Subscription count method</em>'),
      '#weight' => 0,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 2,
      '#attributes' => array('id' => 'webform-signup-limit-component', 'style' => 'display:none;'),
    );
    $form['webform_signup_limit_settings']['webform_signup_limit_component']['webform_signup_limit_count_component'] = array(
      '#type' => 'select',
      '#title' => t('Webform component to use for subscription counting'),
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#options' => $available_components,
      '#default_value' => variable_get('webform_signup_limit_count_component', 0),
    );
    $form['webform_signup_limit_settings']['webform_signup_limit_component']['webform_signup_limit_count_list_max'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum value for webform pre-built list'),
      '#description' => t('Specify the maximum numeric value that can be used in the webform select pre-built list that is made available.'),
      '#required' => FALSE,
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => variable_get('webform_signup_limit_count_list_max', 25),
    );

    $form['webform_signup_limit_settings']['webform_signup_limit_count_component_validate'] = array(
      '#type' => 'checkbox',
      '#title' => t('Validate component value to not cross subscription limit'),
      '#default_value' => variable_get('webform_signup_limit_count_component_validate', 0),
      '#weight' => 3,
    );
  }
  else {
    $form['webform_signup_limit_settings']['webform_signup_limit_settings_empty'] = array(
      '#value' => 'You need to configure the "Event content types" & "Node ID for template node" setting above first',
    );
  }

  /** Date selection settings */
  $form['webform_signup_date_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription date settings'),
    '#description' => t('Configure options for automatically mapping event node & webform submission date entries'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if (!empty($event_types) && $template_node_nid > 0) {
    $available_date_fields = webform_signup_get_content_fields(array('date', 'datestamp', 'datetime'), FALSE, FALSE);
    $date_formats = webform_signup_get_date_formats();
    $form['webform_signup_date_settings']['webform_signup_date_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Subscription date fields for option list'),
      '#description' => t('Specify the CCK fields that can provide data to the Webform select component pre-built option list'),
      '#options' => $available_date_fields,
      '#default_value' => variable_get('webform_signup_date_fields', array()),
    );
    $form['webform_signup_date_settings']['webform_signup_date_future_only'] = array(
      '#type' => 'checkbox',
      '#title' => t('Only show future dates in the date component option list'),
      '#default_value' => variable_get('webform_signup_date_future_only', 0),
    );
    // @TODO: make sure this actually works
    /*$form['webform_signup_date_settings']['webform_signup_date_format'] = array(
      '#type' => 'select',
      '#title' => t('Date format'),
      '#description' => t('Specify the date format to use in the select component'),
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#options' => $date_formats,
      '#default_value' => variable_get('webform_signup_date_format', 'medium'),
    );*/
  }
  else {
    $form['webform_signup_date_settings']['webform_signup_mapping_settings_empty'] = array(
      '#value' => 'You need to configure the "Event content types" & "Node ID for template node" setting above first',
    );
  }


  return system_settings_form($form);
}

/**
 * Get a list of all available CCK fields for a given type
 */
function webform_signup_get_content_fields($types, $disallow_multiple = FALSE, $add_select_option = TRUE) {
  // convert string to array if appropriate
  if (!is_array($types)) {
    $types = array($types);
  }

  $available_fields = array();
  if ($add_select_option) {
    $available_fields[''] = t('--- select ---');
  }

  if (module_exists('content')) {
    $all_fields = content_fields();
    if ($all_fields) {
      foreach ($all_fields as $fieldname => $fieldinfo) {
        if (in_array($fieldinfo['type'], $types)) {
          if ($disallow_multiple) {
            if ($fieldinfo['multiple'] == 0) {
              $available_fields[$fieldname] = $fieldinfo['widget']['label'] . ' (' . $fieldname . ')';
            }
          }
          else {
            $available_fields[$fieldname] = $fieldinfo['widget']['label'] . ' (' . $fieldname . ')';
          }

        }
      }
    }
  }
  return $available_fields;
}

/**
 * Helper function to get available list of webform components on template node
 */
function webform_signup_get_template_node_components() {
  $components = array(0 => t('--- select ---'));
  $template_node = webform_signup_load_template_node();
  foreach ($template_node->webform['components'] as $cid => $component) {
    $components[$cid] = $component['name'];
  }
  return $components;
}

/**
 * Helper function to get available date formats as option list
 */
function webform_signup_get_date_formats() {
  $options = array();
  $formats = date_get_format_types('', TRUE);
  if ($formats) {
    foreach ($formats as $key => $format) {
      $options[$key] = $format['title'];
    }
  }
  return $options;
}