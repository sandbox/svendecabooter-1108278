if (Drupal.jsEnabled) {
  $(document).ready(function() {
    webform_signup_check_val($("input[name=webform_signup_limit_count_method]:checked"));

    $("input[name=webform_signup_limit_count_method]").change(function () {
      webform_signup_check_val($(this));
    });
  });

  function webform_signup_check_val(el) {
    if (el.val() == 'component') {
      $("fieldset#webform-signup-limit-component").show();
    }
    else {
      $("fieldset#webform-signup-limit-component").hide();
    }
  }
}