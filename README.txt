Description:
------------
The Webform 3.x module allows you to assign webforms to any Drupal content type that you want to have signups.
This module allows you to create an (unpublished) webform node as a signup form template
That webform's form components will be made available to each node of the content type you indicated as needing to have signup functionality.

(more info soon)

Usage:
-----
(more info soon)

Author
------
Sven Decabooter (http://drupal.org/user/35369)
The author can be contacted for paid customizations of this module as well as Drupal consulting and development.