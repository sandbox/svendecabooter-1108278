<?php
// $Id$

/**
 * @file
 * Use webforms to attach signup functionality to event nodes
 */

/**
 * Implementation of hook_perm().
 */
function webform_signup_perm() {
  return array('administer webform signup');
}

/**
 * Implementation of hook_menu().
 */
function webform_signup_menu() {
  $items = array();

  $items['admin/settings/webform_signup'] = array(
    'title' => 'Webform signup',
    'description' => 'Webform signup settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webform_signup_admin_settings'),
    'access arguments' => array('administer webform signup'),
    'file' => 'webform_signup.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function webform_signup_theme() {
  return array(
    'webform_signup_date_option' => array(
      'arguments' => array(
        'item' => NULL,
      ),
    ),
  );
}

/**
 * Implementation of hook_nodeapi().
 */
function webform_signup_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if (in_array($node->type, array_filter(variable_get('webform_signup_contenttypes', array())))) {
    if (in_array($node->type, webform_variable_get('webform_node_types'))) {
      switch ($op) {
        case 'load':
          return array('webform_signup_active' => webform_signup_is_active($node->nid));
          break;
        case 'insert':
          webform_signup_event_node_insert($node);
          webform_signup_event_store_info($node);
          break;
        case 'update':
          webform_signup_event_store_info($node);
          break;
        case 'view':
          if (!webform_signup_is_active($node->nid)) {
            unset($node->webform);
          }
          break;
      }
    }
  }
}

/**
 * Check whether signup should be active on a given webform
 */
function webform_signup_is_active($nid) {
  return db_result(db_query("SELECT active FROM {webform_signup} WHERE nid = %d", $nid));
}

/**
 * Implementation of hook_form_alter().
 */
function webform_signup_form_alter(&$form, $form_state, $form_id) {
  $node = $form['#node'];
    if (arg(2) != 'submission') {
    // If count method 'component' is chosen and validation is enabled,
    // check if the entered subscription number is valid
    if (variable_get('webform_signup_limit_count_component_validate', 0) == 1 && variable_get('webform_signup_limit_count_method', 'default') == 'component') {
      if (in_array($node->type, variable_get('webform_signup_contenttypes', array())) && substr($form_id, 0, 20) == "webform_client_form_") {
        $form['#validate'][] = 'webform_signup_webform_validate';
      }
    }
  }

  $webform_types = array_filter(variable_get('webform_signup_contenttypes', array()));
  if (in_array($node->type, $webform_types) && ($form_id == $node->type . '_node_form')) {
    $active = isset($node->nid) ? webform_signup_is_active($node->nid) : 1;
    $form['webform_signup_add_signup_form'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show event signup form'),
      '#default_value' => $active,
    );
  }
}

function webform_signup_event_store_info($node) {
  $nid = $node->nid;
  $active = $node->webform_signup_add_signup_form;
  $existing = db_result(db_query("SELECT nid FROM {webform_signup} WHERE nid = %d", $nid));
  if ($existing) {
    db_query("UPDATE {webform_signup} SET active = %d WHERE nid = %d", $active, $nid);
  }
  else {
    db_query("INSERT INTO {webform_signup} (nid, active) VALUES (%d, %d)", $nid, $active);
  }
}

/**
 * Validation function to check if the number of selected subscriptions are within the allowed limits
 */
function webform_signup_webform_validate($form, &$form_state) {
  $node = $form['#node'];
  $cid = variable_get('webform_signup_limit_count_component', 0);
  if ($form_key = $form_state['webform']['component_tree']['children'][$cid]['form_key']) {
    $value = $form_state['values']['submitted'][$form_key];
    if (is_numeric($value)) {
      $current_subscriptions = webform_signup_get_current_subscription_count($node->nid, 'component');
      $max_subscriptions = webform_signup_get_subscription_limit($node);
      $new_number = $current_subscriptions + $value;
      $places_left = $max_subscriptions - $current_subscriptions;
      if ($new_number > $max_subscriptions) {
        $label = $form['submitted'][$form_key]['#title'];
        $error = format_plural($places_left, 'There is only @count available place left. Please lower your selection for @label', 'There are only @count available places left. Please lower your selection for @label', array('@label' => $label));
        form_set_error('submitted]['. $form_key, $error);
      }
    }
  }
}

/**
 * Reacts on event node insert events, to attach extra data
 */
function webform_signup_event_node_insert(&$node) {
  $template_node = webform_signup_load_template_node($node->language);
  $node->webform = $template_node->webform;
  // Override references to the template node with references to the current node
  $node->webform['nid'] = $node->nid;
}

/**
 * Implementation of hook_preprocess_node().
 *
 * Used to provide variables to nd module
 */
function webform_signup_preprocess_node(&$vars) {
  $node = $vars['node'];
  if (in_array($node->type, webform_variable_get('webform_node_types'))) {
    if (webform_signup_is_active($node->nid)) {
      if (variable_get('webform_signup_use_subscription_limit', 0) == 1) {
        $limit = webform_signup_get_subscription_limit($node);
        if ($limit && $limit > 0) {
          $count_method = variable_get('webform_signup_limit_count_method', 'default');
          $current_num = webform_signup_get_current_subscription_count($node->nid, $count_method);
          if ($current_num >= $limit) { // we reached the limit
            $output = t('Sold out');
          }
          else {
            $output = l(t('Sign up'), 'node/' . $node->nid);
          }
        }
      }
    }
  }
  if ($output) {
    $vars['webform_signup_link'] = $output;
  }
}

/**
 * Load the node to be used as webform template node
 */
function webform_signup_load_template_node($node_language = NULL) {
  global $language;
  if (!$node_language) {
    $node->language = $language;
  }
  if (variable_get('webform_signup_use_template_node', 0) == 1) {
    $nid = variable_get('webform_signup_template_nid', 0);
    if ($nid) {
      $node = node_load($nid);
      if (module_exists('translation_helpers')) {
        $translated_node = translation_helpers_get_translation($node, $node_language);
        if ($translated_node) {
          $node = $translated_node;
        }
      }
      if (in_array($node->type, webform_variable_get('webform_node_types'))) {
        return $node;
      }
    }
  }
  return;
}

/**
 * Implementation of hook_webform_submission_insert().
 */
function webform_signup_webform_submission_insert($node, $submission) {
  webform_signup_check_subscription_limit($node, $submission);
}

/**
 * Implementation of hook_webform_submission_update().
 */
function webform_signup_webform_submission_update($node, $submission) {
  webform_signup_check_subscription_limit($node, $submission, TRUE);
}

/**
 * Implementation of hook_webform_submission_delete().
 */
function webform_signup_webform_submission_delete($node, $submission) {
  webform_signup_check_subscription_limit($node, $submission, TRUE);
}

/**
 * Check whether subscription limit is reached, and deactivate form if so
 */
function webform_signup_check_subscription_limit($node, $submission, $update = FALSE) {
  $current_num = 0;
  if (variable_get('webform_signup_use_subscription_limit', 0) == 1) {
    $limit = webform_signup_get_subscription_limit($node);
    if ($limit && $limit > 0) {
      $count_method = variable_get('webform_signup_limit_count_method', 'default');
      $current_num = webform_signup_get_current_subscription_count($node->nid, $count_method);

      // check if we reached the submission limit with this submission
      if ($current_num >= $limit) {
        // disable form for all roles
        db_query("DELETE FROM {webform_roles} WHERE nid = %d", $node->nid);
      }
      else if ($update) {
        // we might have reached the limit before, so make sure to explicitly
        // re-enable form access, based on template node settings
        $template_node = webform_signup_load_template_node();
        foreach (array_filter($template_node->webform['roles']) as $rid) {
          db_query('INSERT INTO {webform_roles} (nid, rid) VALUES (%d, %d)', $node->nid, $rid);
        }
      }
    }
  }
}

/**
 * Get the current number of subscriptions for a given node ID
 */
function webform_signup_get_current_subscription_count($nid, $method) {
  $current_num = 0;
  switch ($method) {
    case 'default':
      $current_num = db_result(db_query("SELECT count(*) AS num FROM {webform_submissions} WHERE nid = %d", $nid));
      break;
    case 'component':
      $cid = variable_get('webform_signup_limit_count_component', 0);
      if ($cid) {
        $current_num = db_result(db_query("SELECT SUM(data) FROM {webform_submitted_data} WHERE nid = %d AND cid = %d", $nid, $cid));
      }
      break;
  }
  return $current_num;
}

/**
 * Helper function to get the subscription limit field value
 */
function webform_signup_get_subscription_limit($node) {
  $limit_field = variable_get('webform_signup_limit_field', '');
  if (!empty($limit_field)) {
    $limit_value = $node->{$limit_field}[0]['value'];
    if ($limit_value > 0) {
      return $limit_value;
    }
  }
  return 0;
}

/**
 * Implementation of hook_webform_select_options_info().
 */
function webform_signup_webform_select_options_info() {
  $items = array();

  $items['webform_signup_dates'] = array(
    'title' => t('Webform signup: signup dates'),
    'options callback' => 'webform_signup_get_node_dates',
  );

  $items['webform_signup_number_participants'] = array(
    'title' => t('Webform signup: number of participants select list'),
    'options callback' => 'webform_signup_get_participant_selection_list',
  );

  return $items;
}

/**
 * Get the dates for a given node to fill the signup form
 *
 * @TODO:
 * - use proper date formatting
 * - make date field a variable
 */
function webform_signup_get_node_dates() {
  $dates = array();
  $date_fields = variable_get('webform_signup_date_fields', array());

  // Load the node object this webform is attached to
  $node = menu_get_object();
  $arg1 = arg(1);
  if (!$node && arg(0) == 'node' && is_numeric($arg1)) {
    $node = node_load($arg1);
  }

  if (is_object($node) && in_array($node->type, variable_get('webform_signup_contenttypes', array()))) {
    if ($date_fields) {
      foreach ($date_fields as $date_field) {
        if ($node->$date_field) {
          foreach ($node->$date_field as $date) {
            $date_value = NULL;
            if (variable_get('webform_signup_date_future_only', 0) == 1) {
              // @TODO: use date api for this?
              if (strtotime($date['value']) > time()) {
                $date_value = theme('webform_signup_date_option', $date);
              }
            }
            else {
              $date_value = theme('webform_signup_date_option', $date);
            }
            if ($date_value) {
              $dates[] = $date_value;
            }
          }
        }
      }
    }
  }
  if ($dates) {
    return drupal_map_assoc($dates);
  }
  else {
    // return dummy content, as the select components 'options' field is required
    return array('' => t('none available'));
  }
}

/**
 * Helper function to generate an array of number to build a webform pre-built list
 * The maximum number in the range can be set through the settings pages
 */
function webform_signup_get_participant_selection_list() {
  $max = variable_get('webform_signup_limit_count_list_max', 25);
  $listitems = range(1, $max);
  return drupal_map_assoc($listitems);
}

/**
 * Themeable function to render the a date entry for the select component
 * @TODO: there is probably a better way to handle this (formatters?)
 */
function theme_webform_signup_date_option($item) {
  $date_output = '';
  $item_type = isset($item['date_type']) ? $item['date_type'] : (is_numeric($item['value']) ? DATE_UNIX : DATE_ISO);
  $timezone = !empty($item['timezone']) ? $item['timezone'] : date_default_timezone_name();
  $timezone_db = !empty($item['timezone_db']) ? $item['timezone_db'] : 'UTC';
  $date = date_make_date($item['value'], $timezone_db, $item_type);
  if (!empty($date) && $timezone_db != $timezone) {
    date_timezone_set($date, timezone_open($timezone));
  }

  $date_output .= date_format_date($date, 'custom', 'd/m/Y');

  if (!empty($item['value2']) && $item['value'] != $item['value2']) {
    $item['value2'] = trim($item['value2']);
    $date = date_make_date($item['value2'], $timezone_db, $item_type);
    if ($timezone_db != $timezone) {
      date_timezone_set($date, timezone_open($timezone));
    }
    $date_output .= ' - ' . date_format_date($date, 'custom', 'd/m/Y');
  }
  return $date_output;
}