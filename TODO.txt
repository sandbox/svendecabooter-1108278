* fix bug: subscription limit component doesn't work if inside fieldset
* add support for # of places left in error message (see boomgaard)